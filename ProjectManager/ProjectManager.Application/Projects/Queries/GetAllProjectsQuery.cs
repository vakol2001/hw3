﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Application.Users.Queries;
using ProjectManager.Data.Entities;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProjectManager.Application.Projects.Models;

namespace ProjectManager.Application.Projects.Queries
{
    public class GetAllTasksQuery : IRequest<IEnumerable<ProjectModel>>
    {
        public class Handler : IRequestHandler<GetAllTasksQuery, IEnumerable<ProjectModel>>
        {
            private readonly IRepository<Project> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Project> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<IEnumerable<ProjectModel>> Handle(GetAllTasksQuery request, CancellationToken cancellationToken)
            {
                var projects = _repository.Get();
                var mappedProjects = _mapper.Map<IEnumerable<ProjectModel>>(projects);
                return System.Threading.Tasks.Task.FromResult(mappedProjects);
            }
        }
    }
}
