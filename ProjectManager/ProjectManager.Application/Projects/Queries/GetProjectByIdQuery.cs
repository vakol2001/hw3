﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Queries
{
    public class GetProjectByIdQuery : IRequest<ProjectModel>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetProjectByIdQuery, ProjectModel>
        {
            private readonly IRepository<Project> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Project> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<ProjectModel> Handle(GetProjectByIdQuery request, CancellationToken cancellationToken)
            {
                var project = _repository.GetById(request.Id);
                var mappedProject = _mapper.Map<ProjectModel>(project);
                return System.Threading.Tasks.Task.FromResult(mappedProject);
            }
        }
    }
}
