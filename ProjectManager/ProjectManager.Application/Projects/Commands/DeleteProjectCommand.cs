﻿using MediatR;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Commands
{
    public class DeleteProjectCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteProjectCommand>
        {
            private readonly IRepository<Project> _repository;

            public Handler(IRepository<Project> repository)
            {
                _repository = repository;
            }


            public Task<Unit> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
            {
                _repository.Delete(request.Id);
                return Unit.Task;
            }
        }
    }
}
