﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Commands
{
    public class CreateProjectCommand : IRequest<ProjectModel>
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public class Handler : IRequestHandler<CreateProjectCommand, ProjectModel>
        {
            private readonly IRepository<Project> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Project> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<ProjectModel> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
            {
                var projectToAdd = _mapper.Map<Project>(request);
                var project = _repository.Create(projectToAdd);
                var mappedProject = _mapper.Map<ProjectModel>(project);
                return System.Threading.Tasks.Task.FromResult(mappedProject);
            }
        }
    }
}
