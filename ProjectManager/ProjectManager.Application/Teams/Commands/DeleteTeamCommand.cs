﻿using MediatR;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class DeleteTeamCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteTeamCommand>
        {
            private readonly IRepository<User> _repository;

            public Handler(IRepository<User> repository)
            {
                _repository = repository;
            }


            public Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
            {
                _repository.Delete(request.Id);
                return Unit.Task;
            }

        }
    }
}
