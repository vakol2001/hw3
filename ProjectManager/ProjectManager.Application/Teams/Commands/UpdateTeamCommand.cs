﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class UpdateTeamCommand : IRequest<TeamModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public class Handler : IRequestHandler<UpdateTeamCommand, TeamModel>
        {
            private readonly IRepository<Team> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Team> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<TeamModel> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
            {
                var teamToAdd = _mapper.Map<Team>(request);
                var team = _repository.Update(teamToAdd);
                var mappedTeam = _mapper.Map<TeamModel>(team);
                return System.Threading.Tasks.Task.FromResult(mappedTeam);
            }
        }
    }
}
