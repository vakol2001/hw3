﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class CreateTeamCommand : IRequest<TeamModel>
    {
        public string Name { get; set; }


        public class Handler : IRequestHandler<CreateTeamCommand, TeamModel>
        {
            private readonly IRepository<Team> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Team> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<TeamModel> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
            {
                var teamToAdd = _mapper.Map<Team>(request);
                var team = _repository.Create(teamToAdd);
                var mappedTeam = _mapper.Map<TeamModel>(team);
                return System.Threading.Tasks.Task.FromResult(mappedTeam);
            }
        }
    }
}
