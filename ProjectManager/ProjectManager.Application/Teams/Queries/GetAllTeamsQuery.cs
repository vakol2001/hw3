﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Queries
{
    public class GetAllTeamsQuery : IRequest<IEnumerable<TeamModel>>
    {
        public class Handler : IRequestHandler<GetAllTeamsQuery, IEnumerable<TeamModel>>
        {
            private readonly IRepository<Team> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Team> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<IEnumerable<TeamModel>> Handle(GetAllTeamsQuery request, CancellationToken cancellationToken)
            {
                var teams = _repository.Get();
                var mappedTeams = _mapper.Map<IEnumerable<TeamModel>>(teams);
                return System.Threading.Tasks.Task.FromResult(mappedTeams);
            }
        }
    }
}
