﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Queries
{
    public class GetTeamByIdQuery : IRequest<TeamModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetTeamByIdQuery, TeamModel>
        {
            private readonly IRepository<Team> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Team> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<TeamModel> Handle(GetTeamByIdQuery request, CancellationToken cancellationToken)
            {
                var team = _repository.GetById(request.Id);
                var mappedTeam = _mapper.Map<TeamModel>(team);
                return System.Threading.Tasks.Task.FromResult(mappedTeam);
            }
        }
    }
}
