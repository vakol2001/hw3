﻿using AutoMapper;
using ProjectManager.Application.Tasks.Commands;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    class TaskMappingProfile : Profile
    {
        public TaskMappingProfile()
        {
            CreateMap<Task, TaskModel>();
            CreateMap<CreateTaskCommand, Task>();
            CreateMap<UpdateTaskCommand, Task>();
        }
    }
}
