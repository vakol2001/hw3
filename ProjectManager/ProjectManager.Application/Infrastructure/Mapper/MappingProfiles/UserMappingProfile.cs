﻿using AutoMapper;
using ProjectManager.Application.Users.Commands;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserModel>().ForMember("RegisteredAt", opt => opt.MapFrom(u => u.CreatedAt));
            CreateMap<CreateUserCommand, User>();
            CreateMap<UpdateUserCommand, User>();
        }
    }
}
