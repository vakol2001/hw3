﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ProjectManager.Application.Infrastructure.Data.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder SeedData(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var projectRepository = serviceScope.ServiceProvider.GetService<IRepository<Project>>();
            var taskRepository = serviceScope.ServiceProvider.GetService<IRepository<Task>>();
            var teamRepository = serviceScope.ServiceProvider.GetService<IRepository<Team>>();
            var userRepository = serviceScope.ServiceProvider.GetService<IRepository<User>>();

            SeedEntities(projectRepository, "projects.json");
            SeedEntities(taskRepository, "tasks.json");
            SeedEntities(teamRepository, "teams.json");
            SeedEntities(userRepository, "users.json");

            return app;
        }

        private static void SeedEntities<TEntity>(IRepository<TEntity> repository, string file) where TEntity : Entity
        {
            var path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Infrastructure\Data\Json\{file}";
            var entities = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(File.ReadAllText(path));
            repository.CreateRange(entities);
        }
    }
}
