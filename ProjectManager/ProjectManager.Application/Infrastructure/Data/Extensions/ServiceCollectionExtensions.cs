﻿using Microsoft.Extensions.DependencyInjection;
using ProjectManager.Data;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Data.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<Project>, Repository<Project>>();
            services.AddSingleton<IRepository<Team>, Repository<Team>>();
            services.AddSingleton<IRepository<User>, Repository<User>>();
            services.AddSingleton<IRepository<Task>, Repository<Task>>();
            return services;
        }
    }
}
