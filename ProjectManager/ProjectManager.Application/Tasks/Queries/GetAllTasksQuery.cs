﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Queries
{
    public class GetAllTasksQuery : IRequest<IEnumerable<TaskModel>>
    {
        public class Handler : IRequestHandler<GetAllTasksQuery, IEnumerable<TaskModel>>
        {
            private readonly IRepository<Task> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Task> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<IEnumerable<TaskModel>> Handle(GetAllTasksQuery request, CancellationToken cancellationToken)
            {
                var tasks = _repository.Get();
                var mappedTasks = _mapper.Map<IEnumerable<TaskModel>>(tasks);
                return System.Threading.Tasks.Task.FromResult(mappedTasks);
            }
        }
    }
}
