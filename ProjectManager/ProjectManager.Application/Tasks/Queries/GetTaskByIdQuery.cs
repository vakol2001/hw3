﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Queries
{
    public class GetTaskByIdQuery : IRequest<TaskModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetTaskByIdQuery, TaskModel>
        {
            private readonly IRepository<Task> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Task> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<TaskModel> Handle(GetTaskByIdQuery request, CancellationToken cancellationToken)
            {
                var task = _repository.GetById(request.Id);
                var mappedTask = _mapper.Map<TaskModel>(task);
                return System.Threading.Tasks.Task.FromResult(mappedTask);
            }
        }
    }
}
