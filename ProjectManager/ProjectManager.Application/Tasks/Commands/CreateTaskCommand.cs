﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Commands
{
    public class CreateTaskCommand : IRequest<TaskModel>
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }


        public class Handler : IRequestHandler<CreateTaskCommand, TaskModel>
        {
            private readonly IRepository<Task> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<Task> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<TaskModel> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
            {
                var userToAdd = _mapper.Map<Task>(request);
                var user = _repository.Create(userToAdd);
                var mappedTask = _mapper.Map<TaskModel>(user);
                return System.Threading.Tasks.Task.FromResult(mappedTask);
            }
        }
    }
}
