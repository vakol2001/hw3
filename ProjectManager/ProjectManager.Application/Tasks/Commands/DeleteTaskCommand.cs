﻿using MediatR;
using ProjectManager.Data;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Commands
{
    public class DeleteTaskCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteTaskCommand>
        {
            private readonly IRepository<Task> _repository;

            public Handler(IRepository<Task> repository)
            {
                _repository = repository;
            }


            public Task<Unit> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
            {
                _repository.Delete(request.Id);
                return Unit.Task;
            }

        }
    }
}
