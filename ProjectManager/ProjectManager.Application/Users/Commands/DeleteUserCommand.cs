﻿using MediatR;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Commands
{
    public class DeleteUserCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteUserCommand>
        {
            private readonly IRepository<User> _repository;

            public Handler(IRepository<User> repository)
            {
                _repository = repository;
            }


            public Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
            {
                _repository.Delete(request.Id);
                return Unit.Task;
            }

        }
    }
}
