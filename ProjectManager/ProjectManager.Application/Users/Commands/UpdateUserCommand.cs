﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Commands
{
    public class UpdateUserCommand : IRequest<UserModel>
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }


        public class Handler : IRequestHandler<UpdateUserCommand, UserModel>
        {
            private readonly IRepository<User> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<User> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<UserModel> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
            {
                var userToAdd = _mapper.Map<User>(request);
                var user = _repository.Update(userToAdd);
                var mappedUser = _mapper.Map<UserModel>(user);
                return System.Threading.Tasks.Task.FromResult(mappedUser);
            }
        }
    }
}
