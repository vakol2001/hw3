﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Queries
{
    public class GetUserByIdQuery : IRequest<UserModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetUserByIdQuery, UserModel>
        {
            private readonly IRepository<User> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<User> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<UserModel> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
            {
                var user = _repository.GetById(request.Id);
                var mappedUser = _mapper.Map<UserModel>(user);
                return System.Threading.Tasks.Task.FromResult(mappedUser);
            }
        }
    }
}
