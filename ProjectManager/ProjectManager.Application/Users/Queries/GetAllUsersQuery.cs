﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Queries
{
    public class GetAllUsersQuery : IRequest<IEnumerable<UserModel>>
    {
        public class Handler : IRequestHandler<GetAllUsersQuery, IEnumerable<UserModel>>
        {
            private readonly IRepository<User> _repository;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, IRepository<User> repository)
            {
                _repository = repository;
                _mapper = mapper;
            }


            public Task<IEnumerable<UserModel>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
            {
                var users = _repository.Get();
                var mappedUsers = _mapper.Map<IEnumerable<UserModel>>(users);
                return System.Threading.Tasks.Task.FromResult(mappedUsers);
            }
        }
    }
}
