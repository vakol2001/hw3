﻿using Newtonsoft.Json;
using System;

namespace ProjectManager.ConsoleInterface
{
    static class JsonPrinter
    {

        public static void PrintObject(object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            Console.WriteLine(json);
        }
    }
}
