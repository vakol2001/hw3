﻿using ProjectManager.BL;
using System;
using System.Threading.Tasks;

namespace ProjectManager.ConsoleInterface
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var app = new Application();
            await app.SetUpAsync();
            app.Start();
        }
    }
}
