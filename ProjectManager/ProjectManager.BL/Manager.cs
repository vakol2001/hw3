﻿using ProjectManager.BL.Entities;
using ProjectManager.BL.Interfaces;
using ProjectManager.BL.Models;
using ProjectManager.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManager.BL
{
    public class Manager
    {
        IEnumerable<Project> _projects;
        public async System.Threading.Tasks.Task DonwoadDataAsync(string path)
        {
            using IConnectService _connectService = new ConnectService(path);
            var users = await _connectService.GetUsersAsync();
            var projects = await _connectService.GetProjectsAsync();
            var tasks = await _connectService.GetTasksAsync();
            var teams = await _connectService.GetTeamsAsync();

            tasks = tasks.Join(inner: users,
                               outerKeySelector: t => t.PerformerId,
                               innerKeySelector: u => u.Id,
                               resultSelector: (t, u) => t with { Performer = u });

            _projects = projects.GroupJoin(
                                    inner: tasks,
                                    outerKeySelector: p => p.Id,
                                    innerKeySelector: t => t.ProjectId,
                                    resultSelector: (p, ts) => p with { Tasks = ts })
                                .Join(
                                    inner: users,
                                    outerKeySelector: p => p.AuthorId,
                                    innerKeySelector: u => u.Id,
                                    resultSelector: (p, u) => p with { Author = u })
                                .Join(
                                    inner: teams,
                                    outerKeySelector: p => p.TeamId,
                                    innerKeySelector: t => t.Id,
                                    resultSelector: (p, t) => p with { Team = t });
        }

        public Dictionary<Project, int> GetProjectInfoByUserId(int id) // task 1
        {

            return _projects.Where(p => p.AuthorId == id)
                             .ToDictionary(
                                  keySelector: p => p,
                                  elementSelector: p => p.Tasks.Count());
        }

        public IEnumerable<Entities.Task> GetTasksWithShortNameByUserId(int id) // task 2
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == id)
                            .Where(t => t.Name.Length < 45);
        }

        public IEnumerable<TaskModel> GetTasksWichFinishedInCurrentYearByUserId(int id) // task 3
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == id)
                            .Where(t => t.FinishedAt?.Year == DateTime.Now.Year)
                            .Select(t => new TaskModel { Id = t.Id, Name = t.Name });
        }

        public IEnumerable<TeamModel> GetTeamsWithParticipantsOlderThan10() // task 4
        {
            return _projects.Select(p => p.Team)
                            .Distinct()
                            .GroupJoin(
                                inner: _projects.SelectMany(p => p.Tasks)
                                                .Select(t => t.Performer)
                                                .Concat(_projects.Select(p => p.Author))
                                                .Distinct(),
                                outerKeySelector: t => t.Id,
                                innerKeySelector: u => u.TeamId,
                                resultSelector: (t, us) => t with { Participants = us })
                            .Where(t => t.Participants.All(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                            .Select(t => new TeamModel
                            {
                                Id = t.Id,
                                Name = t.Name,
                                Participants = t.Participants.OrderByDescending(u => u.RegisteredAt)
                            });
        }

        public IEnumerable<User> GetSortedUsersWithSortedTasks() // task 5
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Distinct()
                            .GroupBy(t => t.Performer)
                            .Select(g => g.Key with { Tasks = g.OrderByDescending(t => t.Name.Length) })
                            .OrderBy(u => u.FirstName);
        }

        public UserModel GetUserInfo(int id) // task 6
        {

            return _projects.Where(p => p.AuthorId == id)
                            .OrderBy(p => p.CreatedAt)
                            .Select(p => new UserModel
                            {
                                User = p.Author,
                                LastProject = p,
                                LastProjectAmountTasks = p.Tasks.Count(),
                                AmountNotFinishedTasks = _projects.SelectMany(p => p.Tasks)
                                                                    .Count(t => t.PerformerId == p.AuthorId && t.FinishedAt is null),
                                LongestTask = _projects.SelectMany(p => p.Tasks)
                                                        .Where(t => t.Id == id)
                                                        .OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)
                                                        .FirstOrDefault()
                            })
                            .LastOrDefault();
        }

        public IEnumerable<ProjectModel> GetProjectsInfo() // task 7
        {

            return _projects.Where(p => p.Description.Length > 20 || p.Tasks.Count() < 3)
                            .Select(p => new ProjectModel
                            {
                                Project = p,
                                LongestTaskByDescription = p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault(),
                                ShortestTaskByName = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                ParticipantsAmount = _projects.SelectMany(pr => pr.Tasks)
                                                              .Select(t => t.Performer)
                                                              .Concat(_projects.Select(pr => pr.Author))
                                                              .Distinct()
                                                              .Count(u => u.TeamId == p.TeamId)
                            });
        }

    }
}
