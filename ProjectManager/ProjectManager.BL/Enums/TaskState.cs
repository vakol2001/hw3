﻿namespace ProjectManager.BL.Enums
{
    public enum TaskState
    {
        Planning,
        Executing,
        Monitoring,
        Closing
    }
}
