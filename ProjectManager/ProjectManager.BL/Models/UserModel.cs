﻿using ProjectManager.BL.Entities;

namespace ProjectManager.BL.Models
{
    public record UserModel
    {
        public User User { get; init; }
        public Project LastProject { get; init; }
        public int LastProjectAmountTasks { get; init; }
        public int AmountNotFinishedTasks { get; set; }
        public Task LongestTask { get; init; }
    }
}
