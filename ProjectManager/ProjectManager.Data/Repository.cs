﻿using ProjectManager.Data.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ProjectManager.Data
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        static int _count;
        readonly ConcurrentDictionary<int, TEntity> _data = new();

        public TEntity Create(TEntity entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (entity.Id != 0)
            {
                throw new ArgumentException("Entity already have the id", nameof(entity));
            }
            var entityToAdd = entity with { Id = Interlocked.Increment(ref _count) - 1, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now };
            _data[entityToAdd.Id] = entityToAdd;

            return entityToAdd;
        }
        public IEnumerable<TEntity> CreateRange(IEnumerable<TEntity> entities)
        {
            if (entities is null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            return entities.Select(e => Create(e)).ToList();
        }

        public TEntity Delete(int id)
        {
            if (_data.TryRemove(id, out var value))
            {
                return value;
            }

            throw new KeyNotFoundException();
        }

        public TEntity Delete(TEntity entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return Delete(entity.Id);
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null)
        {
            if (filter is null)
            {
                return _data.Select(kvp => kvp.Value).ToList();
            }
            return _data.Select(kvp => kvp.Value).Where(filter).ToList();
        }

        public TEntity GetById(int id)
        {
            return _data[id];
        }

        public TEntity Update(TEntity entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (_data.ContainsKey(entity.Id))
            {
                TEntity entityToUpdate = entity with { UpdatedAt = DateTime.Now };
                _data[entity.Id] = entityToUpdate;
                return entityToUpdate;
            }
            throw new KeyNotFoundException();
        }
    }
}
