﻿using ProjectManager.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProjectManager.Data
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null);
        TEntity GetById(int id);
        TEntity Create(TEntity entity);
        IEnumerable<TEntity> CreateRange(IEnumerable<TEntity> entities);
        TEntity Update(TEntity entity);
        TEntity Delete(int id);
        TEntity Delete(TEntity entity);

    }
}
