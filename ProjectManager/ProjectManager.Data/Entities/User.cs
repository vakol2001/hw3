﻿using System;

namespace ProjectManager.Data.Entities
{
    public record User : Entity
    {
        public int? TeamId { get; init; }
        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string Email { get; init; }
        public DateTime BirthDay { get; init; }
    }
}
