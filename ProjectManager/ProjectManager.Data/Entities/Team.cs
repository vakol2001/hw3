﻿namespace ProjectManager.Data.Entities
{
    public record Team : Entity
    {
        public string Name { get; init; }
    }
}
