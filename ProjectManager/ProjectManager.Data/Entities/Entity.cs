﻿using Newtonsoft.Json;
using System;

namespace ProjectManager.Data.Entities
{
    public abstract record Entity
    {
        [JsonIgnore]
        public int Id { get; init; }
        [JsonIgnore]
        public DateTime CreatedAt { get; init; }
        [JsonIgnore]
        public DateTime UpdatedAt { get; init; }
    }
}
