﻿using System;

namespace ProjectManager.Data.Entities
{
    public record Project : Entity
    {
        public int AuthorId { get; init; }
        public int TeamId { get; init; }
        public string Name { get; init; }
        public string Description { get; init; }
        public DateTime Deadline { get; init; }
    }
}
